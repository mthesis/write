\contentsline {section}{Table of content}{1}{section*.1}%
\contentsline {section}{\numberline {1}Motivation}{3}{section.1}%
\contentsline {section}{\numberline {2}Introduction and literature}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}New physics }{5}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Neuronal networks and autoencoder }{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Graphs }{8}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Graph autoencoder }{10}{subsection.2.4}%
\contentsline {section}{\numberline {3}Basic concepts}{11}{section.3}%
\contentsline {subsection}{\numberline {3.1}Binary classification }{11}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}ROC curve}{11}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Area under the curve}{13}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Datapreperation }{14}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Explaining figures used in this thesis }{15}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Output images}{15}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}AUC Feature maps}{16}{subsubsection.3.3.2}%
\contentsline {section}{\numberline {4}A working graph autoencoder}{18}{section.4}%
\contentsline {subsection}{\numberline {4.1}Graph neural networks }{18}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Tensorproducts}{18}{subsubsection.4.1.1}%
\contentsline {subsection}{\numberline {4.2}The compression algorithm }{19}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}the decompression algorithm }{19}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Our model setup }{20}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Our choice not to use biases}{20}{subsubsection.4.4.1}%
\contentsline {subsection}{\numberline {4.5}Choosing the rigth loss }{21}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}$L_{2}$ loss}{21}{subsubsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.2}$L_{n}$ loss}{22}{subsubsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.3}Image like losses}{23}{subsubsection.4.5.3}%
\contentsline {subsection}{\numberline {4.6}Difficulties when evaluating a model }{24}{subsection.4.6}%
\contentsline {subsubsection}{\numberline {4.6.1}AUC scores}{24}{subsubsection.4.6.1}%
\contentsline {subsubsection}{\numberline {4.6.2}Losses}{25}{subsubsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.3}Images}{25}{subsubsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.4}oneoff width}{25}{subsubsection.4.6.4}%
\contentsline {subsection}{\numberline {4.7}Evaluating the autoencoder }{26}{subsection.4.7}%
\contentsline {subsubsection}{\numberline {4.7.1}4 nodes}{26}{subsubsection.4.7.1}%
\contentsline {subsubsection}{\numberline {4.7.2}9 nodes}{27}{subsubsection.4.7.2}%
\contentsline {subsection}{\numberline {4.8}Evaluating the classifier }{28}{subsection.4.8}%
\contentsline {subsubsection}{\numberline {4.8.1}4 nodes}{28}{subsubsection.4.8.1}%
\contentsline {subsubsection}{\numberline {4.8.2}9 nodes}{29}{subsubsection.4.8.2}%
\contentsline {section}{\numberline {5}Apparent questions}{32}{section.5}%
\contentsline {subsection}{\numberline {5.1}Scaling the network size }{32}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Problems in scaling}{32}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Scaling through batches}{32}{subsubsection.5.1.2}%
\contentsline {subsubsection}{\numberline {5.1.3}Scaling through dense networks}{33}{subsubsection.5.1.3}%
\contentsline {subsubsection}{\numberline {5.1.4}C addition}{34}{subsubsection.5.1.4}%
\contentsline {subsubsection}{\numberline {5.1.5}Scaling through losses}{36}{subsubsection.5.1.5}%
\contentsline {subsection}{\numberline {5.2}Simplicity and invertibility }{37}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Simplicity}{37}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Invertibility}{41}{subsubsection.5.2.2}%
\contentsline {section}{\numberline {6}Normalization}{42}{section.6}%
\contentsline {subsection}{\numberline {6.1}Introudicing normalization for autoencoder }{42}{subsection.6.1}%
\contentsline {subsubsection}{\numberline {6.1.1}The meaning of complexity}{42}{subsubsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.2}How to normalise an autoencoder}{42}{subsubsection.6.1.2}%
\contentsline {subsection}{\numberline {6.2}Using this normalization }{45}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}Improving the AUC scores for normalized networks}{48}{subsubsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2}Scaling in normalized networks}{48}{subsubsection.6.2.2}%
\contentsline {subsubsection}{\numberline {6.2.3}Improving the normalization even further}{48}{subsubsection.6.2.3}%
\contentsline {section}{\numberline {7}Mixed networks}{51}{section.7}%
\contentsline {subsection}{\numberline {7.1}Oneoff networks }{51}{subsection.7.1}%
\contentsline {subsubsection}{\numberline {7.1.1}Oneoff quality}{52}{subsubsection.7.1.1}%
\contentsline {subsection}{\numberline {7.2}Latent space oneoff learning }{53}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}A final classifier }{54}{subsection.7.3}%
\contentsline {subsubsection}{\numberline {7.3.1}Trained on QCD}{54}{subsubsection.7.3.1}%
\contentsline {subsubsection}{\numberline {7.3.2}Trained on top}{55}{subsubsection.7.3.2}%
\contentsline {subsection}{\numberline {7.4}Scaling with oneoff networks }{57}{subsection.7.4}%
\contentsline {subsubsection}{\numberline {7.4.1}Scaling in batches}{57}{subsubsection.7.4.1}%
\contentsline {subsubsection}{\numberline {7.4.2}Scaling without batches}{58}{subsubsection.7.4.2}%
\contentsline {section}{\numberline {8}Applying this model to other datasets}{61}{section.8}%
\contentsline {subsection}{\numberline {8.1}Ligth dark matter }{61}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Other datasets }{64}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Quark or gluon}{64}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Leptons}{65}{subsubsection.8.2.2}%
\contentsline {subsection}{\numberline {8.3}Cross comparisons }{67}{subsection.8.3}%
\contentsline {section}{\numberline {9}Conclusion}{69}{section.9}%
\contentsline {subsection}{\numberline {9.1}Outlook }{70}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Acknowledgements }{71}{subsection.9.2}%
\contentsline {section}{Appendices}{72}{subsection.9.2}%
\contentsline {section}{\numberline {A}Understanding certain choices}{72}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Changing the input feature space }{72}{subsection.A.1}%
\contentsline {subsection}{\numberline {A.2}Is it a good idea to relearn the graph at each step? }{73}{subsection.A.2}%
\contentsline {subsection}{\numberline {A.3}The consequences of sorting outputs by lpt }{74}{subsection.A.3}%
\contentsline {subsection}{\numberline {A.4}The usage of a batchNormalization layer in the middle of the graph autoencoder }{75}{subsection.A.4}%
\contentsline {subsection}{\numberline {A.5}Changing the definition of the transverse momentum input}{77}{subsection.A.5}%
\contentsline {subsection}{\numberline {A.6}Comparing our graph update layer to particleNet }{77}{subsection.A.6}%
\contentsline {section}{\numberline {B}Experiments using graph autoencoder}{78}{appendix.B}%
\contentsline {subsection}{\numberline {B.1}Variating the compression size }{78}{subsection.B.1}%
\contentsline {subsection}{\numberline {B.2}Things we learned from implementing a Graph Autoencoder in tensorflow and keras }{79}{subsection.B.2}%
\contentsline {subsubsection}{\numberline {B.2.1}Overflow in angular differences, and how to solve it}{79}{subsubsection.B.2.1}%
\contentsline {subsubsection}{\numberline {B.2.2}How to deal with NANs}{79}{subsubsection.B.2.2}%
\contentsline {subsubsection}{\numberline {B.2.3}Why relus are great}{80}{subsubsection.B.2.3}%
\contentsline {subsection}{\numberline {B.3}Metrik analysis }{80}{subsection.B.3}%
\contentsline {subsection}{\numberline {B.4}How topK works exactly}{81}{subsection.B.4}%
\contentsline {subsubsection}{\numberline {B.4.1}Problems}{82}{subsubsection.B.4.1}%
\contentsline {subsubsection}{\numberline {B.4.2}Why topK might actually not be the best idea}{83}{subsubsection.B.4.2}%
\contentsline {subsection}{\numberline {B.5}Trainingsize, and why graph autoencoder don`t care about it }{84}{subsection.B.5}%
\contentsline {subsection}{\numberline {B.6}Why autoencoder reproduce mean values }{85}{subsection.B.6}%
\contentsline {section}{\numberline {C}Overview of less useful networks}{87}{appendix.C}%
\contentsline {subsection}{\numberline {C.1}Failed approaches }{87}{subsection.C.1}%
\contentsline {subsubsection}{\numberline {C.1.1}Trivial models}{87}{subsubsection.C.1.1}%
\contentsline {subsubsection}{\numberline {C.1.2}Minimal models}{87}{subsubsection.C.1.2}%
\contentsline {subsection}{\numberline {C.2}The first graph autoencoder that could be considered working }{88}{subsection.C.2}%
\contentsline {subsubsection}{\numberline {C.2.1}Training setup}{88}{subsubsection.C.2.1}%
\contentsline {subsubsection}{\numberline {C.2.2}Results}{89}{subsubsection.C.2.2}%
\contentsline {subsection}{\numberline {C.3}Improving autoencoder }{90}{subsection.C.3}%
\contentsline {subsubsection}{\numberline {C.3.1}Training setup}{90}{subsubsection.C.3.1}%
\contentsline {subsubsection}{\numberline {C.3.2}Results}{91}{subsubsection.C.3.2}%
\contentsline {subsection}{\numberline {C.4}Improving autoencoder even further? }{93}{subsection.C.4}%
\contentsline {subsubsection}{\numberline {C.4.1}Physical intuition behind the encoding algorithm}{93}{subsubsection.C.4.1}%
\contentsline {subsubsection}{\numberline {C.4.2}Better encoding}{93}{subsubsection.C.4.2}%
\contentsline {subsubsection}{\numberline {C.4.3}Better decoding}{94}{subsubsection.C.4.3}%
\contentsline {subsection}{\numberline {C.5}The compression algorithm that we wish we would be able to write }{96}{subsection.C.5}%
\contentsline {section}{\numberline {D}More problems while writing a graph autoencoder}{98}{appendix.D}%
\contentsline {subsection}{\numberline {D.1}Choosing the rigth compression size }{98}{subsection.D.1}%
\contentsline {subsection}{\numberline {D.2}Building identities out of graphs }{98}{subsection.D.2}%
\contentsline {subsection}{\numberline {D.3}Is permutation invariance good or bad? }{100}{subsection.D.3}%
\contentsline {subsection}{\numberline {D.4}Why use graph autoencoder }{100}{subsection.D.4}%
\contentsline {subsection}{\numberline {D.5}Why not to use graph autoencoder }{100}{subsection.D.5}%
\contentsline {subsubsection}{\numberline {D.5.1}Reproduding vs classifing quality}{100}{subsubsection.D.5.1}%
\contentsline {section}{\numberline {E}Understanding Oneoff networks with more precision}{106}{appendix.E}%
\contentsline {subsection}{\numberline {E.1}Other algorithms }{106}{subsection.E.1}%
\contentsline {subsubsection}{\numberline {E.1.1}Support vector machines}{106}{subsubsection.E.1.1}%
\contentsline {subsubsection}{\numberline {E.1.2}K neirest neighbours}{107}{subsubsection.E.1.2}%
\contentsline {subsubsection}{\numberline {E.1.3}Isolation forests}{107}{subsubsection.E.1.3}%
\contentsline {subsection}{\numberline {E.2}Different algorithms for latent space training }{108}{subsection.E.2}%
\contentsline {subsubsection}{\numberline {E.2.1}SVM}{108}{subsubsection.E.2.1}%
\contentsline {subsubsection}{\numberline {E.2.2}Isolation forest}{108}{subsubsection.E.2.2}%
\contentsline {subsubsection}{\numberline {E.2.3}k neirest neighbour}{108}{subsubsection.E.2.3}%
\contentsline {subsubsection}{\numberline {E.2.4}Oneoff}{108}{subsubsection.E.2.4}%
\contentsline {subsection}{\numberline {E.3}Oneoff math }{109}{subsection.E.3}%
\contentsline {subsection}{\numberline {E.4}Self improving oneoff networks }{111}{subsection.E.4}%
\contentsline {subsubsection}{\numberline {E.4.1}Oneoff outside of physics}{113}{subsubsection.E.4.1}%
\contentsline {subsubsection}{\numberline {E.4.2}Physical interpretability for oneoff networks}{114}{subsubsection.E.4.2}%
\contentsline {subsection}{\numberline {E.5}How an oneoff network can become noninvertible }{115}{subsection.E.5}%
\contentsline {subsection}{\numberline {E.6}Why c addition might not be perfect }{117}{subsection.E.6}%
\contentsline {section}{\numberline {F}Other usecases for grapa}{118}{appendix.F}%
\contentsline {subsection}{\numberline {F.1}Abnormal account detection for social networks }{118}{subsection.F.1}%
\contentsline {subsubsection}{\numberline {F.1.1}Datageneration}{118}{subsubsection.F.1.1}%
\contentsline {subsubsection}{\numberline {F.1.2}Training}{119}{subsubsection.F.1.2}%
\contentsline {subsubsection}{\numberline {F.1.3}Whats next}{120}{subsubsection.F.1.3}%
\contentsline {subsection}{\numberline {F.2}Accelarating molecular networks through pooling }{121}{subsection.F.2}%
\contentsline {subsubsection}{\numberline {F.2.1}Datageneration}{121}{subsubsection.F.2.1}%
\contentsline {subsubsection}{\numberline {F.2.2}Training}{121}{subsubsection.F.2.2}%
\contentsline {subsubsection}{\numberline {F.2.3}Whats next?}{122}{subsubsection.F.2.3}%
\contentsline {subsection}{\numberline {F.3}High level machine learning and feynman diagramms }{122}{subsection.F.3}%
\contentsline {subsubsection}{\numberline {F.3.1}Data generation}{122}{subsubsection.F.3.1}%
\contentsline {subsubsection}{\numberline {F.3.2}Training}{123}{subsubsection.F.3.2}%
\contentsline {subsubsection}{\numberline {F.3.3}Whats next?}{126}{subsubsection.F.3.3}%
\contentsline {subsection}{\numberline {F.4}Graph like generators and onoff initializers }{127}{subsection.F.4}%
\contentsline {subsubsection}{\numberline {F.4.1}Data generation}{127}{subsubsection.F.4.1}%
\contentsline {subsubsection}{\numberline {F.4.2}Training}{127}{subsubsection.F.4.2}%
\contentsline {subsubsection}{\numberline {F.4.3}Whats next?}{128}{subsubsection.F.4.3}%
\contentsline {section}{\numberline {G}Additional Figures}{129}{appendix.G}%
\contentsline {section}{List of Figures}{133}{section*.140}%
\contentsline {section}{List of Tables}{137}{section*.141}%
\contentsline {section}{References}{137}{section*.142}%
