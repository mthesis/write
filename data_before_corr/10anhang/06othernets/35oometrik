<subsubsection title="Some physical interpretability for oneoff networks" label="oometrik">
Another example, why oneoff networks migth be quite useful, comes from our experiments to understand them more <ignore>(see chapter (ENTER chapter))</ignore>. Instead of constructing arbitrary features by utilising deep networks, the algorithm used here only combines input features in a linear way. The data we work on here is provided by cern open data as two lepton events from the 2010 datasets. Momentum 4 vectors of muons<cite setmuon> as background and of electrons<cite setelectron> as signals. These 4 vectors are multiplied with a linear metrik, reducing it into one dimension, that is evaluated to minimize #(abs(p**(mu)*g_mu_nu*p**(nu))-1)**2#. This results in the network learning the following metrik
<table caption="Learned metrik values of oneoff networks trained on muon events" label="oneoffmuon" c="5" mode="classic">
<hline>
<tline " ~#E#~#p_1#~#p_2#~#p_3#">
<hline>
<tline #E#~-0.4997~0.0011~-0.0002~0.0002>
<tline #p_1#~0.0011~0.5069~0.0014~-0.0008>
<tline #p_2#~-0.0002~0.0014~0.4930~-0.0006>
<tline #p_3#~0.0002~-0.0008~-0.0006~0.4998>
<hline>

</table>

As you see, the result is very similar to a minkowski metrik: The nondiagonal parts are zero in the range of numerical uncertainity (and symmetric for 5 digits behind the commata), the signs are randomly this way, because of the absolut value in the loss function and the absolute value of the diagonal parts scales the resulting expected output of #1# that the loss expects. Other than this, this simple network is able to understand itself, that characterising a particle is best done through what we call its mass. That beeing said, the AUC score is not optimal, only reaching #0.5988#, even though we can improve this, by assuming the metrik to be strictly diagonal, which results in a learned metrik of
<table caption="Learned metrik values for a diagonal metrik oneoff networks trained on muon events" label="oneoffmuondiag" c="4" mode="free">
<hline>
<tline "#E#~#p_1#~#p_2#~#p_3#">
<hline>
<tline 1.4198~-1.4130~-1.4151~-1.4197>
<hline>

</table>
As you see, this still results in a minkowski metrik like result. This time with a flipped sign, and a different scale, which is just a feature of the implementation. Most importantly, this simplified metrik definition, including less noise, results in a much higher auc value of #0.8007#<note you could ask yourself, why we use muons as background events: This is because the relative uncertainity of each electron mass value is much bigger, since the mass is more than two orders of magnitude smaller. Training a (only diagonal) network like this, still results in a minkowski like metrik (#-0.0058#,#0.0043#,#0.0043#,#0.0058#), but the auc value is way worse reaching only #0.5003# as the expected mean value has way less physical meaning>
