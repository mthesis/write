<subsection title="Solving Invertibility through normalization" label="normalization">

When we remove trivial features from our data, we can solve the problem of our network only learning those. This we try in this chapter.

<subsubsection title="The Meaning of Complexity" label="complexity">
<ignore>Since there is a Model, that can reach an even better quality then a Autoencoder, by just comparing the Angular Data to zero(see chapter <ref simplicity>), it stands to reason, that these Autoencoder work in a similar way, which of course would not make them Invertible<note a model comparing just angles to zero, would be the same, if trained on QCD or on top jets>.</ignore>
Since models seem to be not invertible, since they contain a good trivial model (see chapter <ref simplicity>), it stands to reason, that we migth get an invertible model by removing this trivial model from it. This would also make these models more general, since they cannot rely on trivial information to perform well.
We can remove any kind of Data feature, that allows for this trivial comparison, by normating your input, as chapter <ref simplicity> shows, that the width of the angular input distribution is the trivial feature<note it would be enough to normate angular information, but to gain generality, we also normate the momentum information>.
This method has one obvious drawback: Not only do we actively remove Information, which will hurt the Performance, but we remove the Information, that generates most of the classification quality. This means, even if the resulting Classifier is invertible, it will look way worse than the trivial one. This does not mean that these networks are less useful, as we trade quality on the task of QCD v top jets against generality that is desperately needed in the networks of chapter <ref secgae>, but an approach not loosing quality would clearly be better (see out second solution in chapter <ref secmixed>).


<subsubsection title="How to normalise an Autoencoder" label="normprobs">
One thing, We did not realise before trying to normalise the Input Datapoints, is that simply demanding that the mean is zero and the standart deviation is one, just does not work. This migth be an effect that is most important when we talk about small Networks<note Networks with a low amount of Input Particles>, but is still somewhat of an effect in every Network, and becomes important in chapters <ref oneoff> and <ref secmixed>. The Problem is, that by demanding a value to be fixed, we remove the size of the Input Space, and by having an autoencoder that only reduces 12+flag Information onto 9 Values, this means, we allow the Network to trivially learn 3 Informations per set Feature<note 3, since there are 3 Variables which mean and or standart deviation we fix>, and so by setting the standart deviation and mean to be fixed, the autoencoder can trivially learn to compress 12+flag onto 6 Values<note ignoring flag for now, three Values is always enough to encode 4 flag Values, since the four three flag Values are neirly always one (the jet with the lowest number of particles has 3 particles in our trainingsset)>, which is below the size of compression space. In practice this is not so easy as there is no garantee that this minima is found, since the Graph Structure does not neccesarily help for this kind of transformation(see appendix <ref identities>), but training this kind of Network definitely does not result in the Model gaining any Classification Power. This can be seen in the corresponding Feature Map
<i f="aucmap1011" wmode="True">(1011..maybe not the best)Aucmap for normally normated networks, showing nothing useful being learned</i>
This migth seem now, as if there is a trivial solution: just reduce the compression size accordingly, but this has three problems
<list>
<e>First, it is not completely trivial to misuse the normalization (Think of the standart deviation, there is a formula giving you Information about the 4th Value, given the first three. But even if we ignore the mean as beeing 0, this formula still involves squares and roots, which the Network has to learn, and even then, there are always two possibilities for the resulting Value.). So assuming that this is trivial, and that the Network will always learn it garantueed, would be wrong</e>
<e>Even if this is learned, this would not be enough: the network still has to compress this Information further and this can lead to Situations in which the Network has to decide between learning the easy Compression and the learning the interresting Compression. In this Situations it will probably always learn the trivial one</e>
<e>Trying to compress Data with removed Information further is not as easy as compressing non removed Information. Think of two Values distributed between #0# and #10#: For example #4# and #6#, or, after setting the mean to be #0#: #-1# and #1#. In #4# and #6# the Network can still decide to just average both Values and get an mediocre prediction of #5#, which still describes these values in a way. But if after removing the mean, the Network still averages both Values, the predicted #0# is fairly useless<note Please note, that the difference between a good normalization and a bad one is just physical Intuition. For example, we still set the mean of the Jet angles to be zero, just because the direction relative to the Measurement should be unimportant>. From this we conclude, that simply subtracting each fixed Value from the compression size does not work, as we would expect less good Classifier.</e>
</list>

So what we need, is a better way of normalising the Input Data. From our thougths above, this new method should satisfy these conditions:
<list>
<e>Translation Invariance: #Eq(n(x),n(x+a))#</e>
<e>Scale Invariance:#Eq(n(x),n(a*x))#</e>
<e>No fixed features: you can not write any #f(x)# so that #Eq(f(n(x)),0)# for every #x# is given</e>
</list>

The first two Rules are obvious, since we want to use this, to remove any size Information, and third rule would solve the problem of an autoencoder focussing on normalization artefacts<note this last rule would actually be solved by demanding the standart deviation to be constant>.

All three Rules<note except for scale invariance with #LessThan(a,0)#> are solved by the following 3 normalization Steps (#x# is the Input, #n# the Output of the normalization method)

##Eq(y,x-mean(x))##
##Eq(z,y-mean(abs(y)))##
##Eq(n,z/(max(abs(y))+0.001))##

Here the definitions of #y# and #n# assert Translation and Scale Invariance respectively, while #n# and #z# remove any kind of Artefacts. Why they do this can be easily understand for #n#, by diving through the maximum value instead of the standart deviation: The only relation given is, that if none of the first three values is either #+1# or #-1#, the last Value is either #+1# or #-1#. And even if we ignore that misusing this relation would be quite complicated to implement for a Neural Network <note there are uncertainities: if you have a value of #0.997#, is there still a #1# remaining?> there is no way to differenciate between #1# and #-1# in general<note You could say, that this normalization uses the Problem of mean reproducing Networks (chapter <ref ameans>) to its benefit, by making the errors of a #1# or #-1# guessing Network bigger than every other possible distance. But this is a bit more complicated through the definition of #z#, since this kind of outputs have a clearly negative bias>. Also dividing through the maximum Value is generally a good Idea compared to dividing through the standart deviation, since it only divides by zero when every value is zero, and not when every Value is the same, which is more probable<note There is a small constant in our definition to remove this divergences, but this still removes some big gradients>. Also dividing by the maximum is generally a bit faster, while resulting in less nans (chapter <ref nans>). The other definition is less easily understandable: First note, that it does not violate the first two rules, since #y# is already Translation Invariant, #z# is too, and since every #z(y)# is scale Invariant, Scale Invariance is also given<note You migth notice, that this is only the case for positive multiplicators, but we think this is an acceptable compromise for fullfilling Rule 3. Also you could also argue, that we only want to remove nonphysical Information, and since for #p_T# this is not a Problem (since #p_T# is positive and it has a peculiar shape), this is only interresting for angular Information, and Parity is broken>. Now consider only the definition of #z# and two different vectors #y# given by #Matrix([[1,0]])# and #Matrix([[1,1]])#. Both result in the same first component, but different second components, so there is no Information from the first Value to the second, and thus Rule 3 is satisfied. Less theoretically, a Network using this kind of normalization has Classification Power:

<ignore><i f="aucmap928" wmode="True">(928..definitely take some top one)An AUC map for a better normated network</i></ignore>

<subsubsection title="Using this normalization" label="usenorm">
Using this kind of normalization, 4 node Networks are invertible. And not only this, but also most Features are Invertible.
<i f="aucmap677" f2="aucmap928" wmode="True">invertible 4node network auc maps achieved by a better normalization</i>
But the Quality suffers
<i f="drtoptagging" wmode="True">(generate later for computational reasons)double roc curve for invertibility of normated networks</i>
and there are other consequences of the fact that this Network actually has to learn something nontrivial:
First, we were forced to increase the Size of the compressed Feature Space from 5 to 9. This makes sense, as a Network that compares angles to zero, has to just reconstruct zeros in each Angle, and thus only has to save #p_T#<note And maybe flag, but as seen later in this chapter and more in chapter <ref oneoff>, this is usually not actually be the case>, needing only a smaller latent space.
Also Networks, that before were very reproducable in their training<note which makes sense, as they always just needed to learn to ignored the angles> are now less stable, and often vary their loss<note remember that the l2 loss goes quadratic in changes of the Inputs> over about one order of magnitude. Interrestingly, this variation shows a clear relation between the loss, and the Classification Quality
<i f="rtvl 0 2 4 5 6 7 8 9" wmode="True">(from c3pp atm)Relation between the network loss and the AUC score</i>
This relation is great, since it means, that finding a better autoencoder, automatically results in a better Classifier, and we thus can focus completely on improving the Autoencoder. 
Also by looking at this relation, we are able to justify the new compression size
<i f="none" wmode="True">(needs to be generated a bit later sadly for computational reasons)justification of the new compression size, auc vs loss for compression size 8 and 9</i>
since this is the first compression size, at which the Network becomes Invertible.
This variation of the Networks is the reason we started training as long as described in chapter <ref setup>, as this solves the reproducabilitys: As you see
<i f="none" wmode="True">(computational reasons...)comparison of reproducability for earlystopping and minepoch</i>
training for 1000 epochs, and then until the loss increases for 250 epochs, results in the most predictable, and qualitative results. This  migth a bit much, so 500 Epochs and a patience of 100 further epochs have to do.<ignore> In later Models, we reduce this to just stopping the training after 100 consecutive Epochs of not falling losses, to keep the Training time also managable.</ignore>








