<subsection title="Metrik analysis" label="ametrikana">

As explained in appendix <ref atopkhow>, our topK algorithm, on which all graphs are based, uses a learnable diagonal metric, which is used to define similarities in the network. This metric can be extracted to understand this sense of similarity. Figure <refi metrik00> shows that unnormalized networks use the angular differences between nodes to define similarity. Interrestingly figure <refi metrik725> suggests that using a normalization changes this. Now networks only use one angle, and a negative metric value for the other one. This means that two nodes are more similar the more one angle is different, but also the more different the other is.

<i f="metrik00" wmode="True" label="metrik00">Typical metrik of unnormalized networks</i>
<i f="metrik725" wmode="True" label="metrik725">Typical metrik of normalized networks</i>

For a metrik on different features see appendix <ref afeature>.

<ignore>
Finally, you can apply this metrik analysis to networks with multiple inputs: As expalained before in chapter <ref data>, adding more highly related input features does not help you in creating better classifier, since you demand the network to learn those relations. This you can also see here, since more important self interactions, mean less important neighbour interactions, and thus less focus on the graph, resulting in a less clear definition of similarity
</ignore>


