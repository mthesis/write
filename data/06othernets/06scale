<subsection title="Scaling with oneoff networks" label="scale3">

<subsubsection title="Scaling in batches" label="ooscalebatch">
The batches considered in chapter <ref normimpro> are now all invertible, as figure <refi ooinv> shows.
<i f="m4scalesep" wmode="True" label="ooinv"> Invertibility of batches in oneoff networks</i>
Here you see a much more interesting relation compared to before. The variance of each AUC score grows with the batch index, which is expected as they are more random, but some networks actually beat the AUC score of the first batch (batch 3 has an event below #0.15#). This is a result of the number of particles in each jet becoming a feature at some point. You see this, by noticing that the relation between AUC and batch number is not linear: The AUC's for the second batch might even be some of the worst, even though they should have the second most information next the first batch. Sadly this nonlinear relation makes combining batches hard.

<subsubsection title="Scaling without batches" label="finalscale">

From a technical standpoint, bigger networks don't train as well, since their loss becomes NAN at some point. This we can fix for now, by giving up two things: We cannot use a learnable graph anymore, and we train on fewer data. Using a fixed fully connected graph is usually not a good idea, as it seems to slow down the training, but this also removes a lot of NANs<note it is always sadly possible for a network to NAN, which makes debugging harder. Removing the topK algorithm seems to make them appear mostly earlier, resulting in NANs appearing either in the first epochs or not at all, and thus allowing us to not waste any time training failing networks>. Using less data should not matter to much, since for 4 nodes appendix <ref asize> shows that it does not change anything to reduce the number of training samples to #5000# QCD jets. This removes fewer NANs, but has the added effect of accelerating the training a bit. It is also useful to use the normalization from chapter <ref normalization>, as not using it seems to produce much more NANs.

We train with a batch size of #100# and a learning rate of #0.003# for at least #500# Epochs and afterwards with patience of #100# Epochs an autoencoder compressing 16 nodes twice by a factor of 4 into one node with dimension 36 building our latent space. Also, between each compression step, there are 3 graph update steps. This results in the training history shown in figure <refi hist1583>. This training took more than 58 hours training on a cpu<note Training on a gpu would accelarate this quite a lot. We expect a factor between 3 and 5, but since this still would not make gpus possible in our computation quota, we use cpus> <note Training a 4 node network takes about #3# hours, so the quadratic scaling expected for the graph update layer expects a training time of about #48# hours. This difference is most prominent, when you consider that we need train 4 node networks on #50000# training jets. You migth explain it by our model being to easy (we have more than just graph update layers) or by our implementation not being as fast as possible>.
<i f="history1583" wmode="True" label="hist1583">Training history for a 16 node network trained on QCD</i>
More importantly, the reconstruction works fairly well, as figures <refi sd1583> and <refi pt1583> show.
<i f="simpledraw1583" wmode="True" label="sd1583">Angular reconstruction for a 16 node network trained on QCD</i>
<i f="ptdraw1583" wmode="True" label="pt1583">Momentum reconstruction for a 16 node network trained on QCD</i>

Without oneoff networks, the classification quality is also better than our best AUC on 4 nodes (#0.635#). We move figures <refi roc1583> and <refi rec1583> to the appendix, but they show an AUC value of about #0.7#.

Using oneoff network this AUC falls to #0.55# (see figure <refi seproc1583>). This might seem like oneoff networks are not as good as we assumed before, but this is actually not the case. Consider the same training done now on top jets. The training history (figure <refi hist1590>) and the reconstructions (figures <refi sd1590> and <refi pt1590>) are very similarly good, which is why you find them in the appendix.
The problem lies in the ROC curves (figures <refi roc1590> and <refi rec1590>). They reach an AUC score of #0.64#, and we thus would have networks that are not invertible.
We think, that these networks are not invertible (even though we use normalization) because the normalization has a much lower effect: On 4 nodes, removing two values means removing #1/2# of all values, on 16 nodes this only means removing #1/8# of all values. So, since the trivial difference is contained in each particle, and slightly differently for each of the particles, removing 2 values might remove some width, but in the substructure there is still enough contained for the network to only use a triviality.
So we need to use our improved way of handling trivialities: Using oneoff networks here results in an AUC score of #0.48# (see figure <refi seproc1590>) and at least making this network invertible.

These terrible AUC scores show that simply solving the computation challenges of more node networks is not enough: We think that by adding nodes that are more and more random, the autoencoder focuses on reconstructing them more than about the first nodes. But since in these initial nodes most of the classification power is contained, this just weakens the classifier. So you would need to also keep the focus of the network right.
One way of doing this, would be weighting your loss function, but our experiments with losses that are functions of the node index or the transverse momentum only worsened the reconstruction quality (see chapter <ref scaleloss>).



