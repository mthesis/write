<subsection title="solving Invertibility through Normation" label="normation">

<subsubsection title="The Meaning of Complexity" label="complexity">
Since there is a Model, that can reach a similar quality as an good Autoencoder, by just comparing the Angular Data to zero(see Chapter <ref simplicity>, it stands to reason, that these Autoencoder work in a similar way, which of course would not make them Invertible<note a model comparing just angles to zero, would be the same, if trained on qcd or on top jets>.
We can test this Hypothesis, by just removing any kind of Data difference, that allows for this trivial comparison, which, as shown in Chapter <ref simplicity> is the width of the Input Distribution: In other words: What we need is some kind of Normation.
This method has one obvious drawback: Not only do we actively remove Information, which will hurt the Performance, but we remove the Information, that generates most of the quality. This means, even if the resulting Classifier is invertible, it will probably look way worse than the trivial one, even if it is physically much more useful.


<subsubsection title="How to normalise an Autoencoder" label="normprobs">
One thing, We did not realise after triying to normalise my Input Datapoints, is that simply demanding that the mean is zero and the standart deviation is one, just does not work. This migth be an effect that is most important when we talk about small Networks<note Networks with a low amount of Input Particles>, but is still somewhat of an effect in every Network, and becomes important in Chapters <ref oneoff> and <ref secmixed>. The Problem is, that by demanding a value to be fixed, we remove one information from the Input Space, and by having an autoencoder that only reduces 12+flag Information onto 9 Values, this means, we allow the Network to trivially learn 3 Informations per set Feature<note 3, since there are 3 Variables which mean and standart deviation we fix>, and so by setting the standart deviation and mean to be fixed, the autoencoder can trivially learn to compress 12+flag onto 6<note ignoring flag for now, three Values is always enough to encode 4 flag Values, since the four three flag Values are basically always one (the jet with the lowest number of particles has 3 particles in our trainingsset)> Values, which is below our compression space. In reality things are not so easy. There is no garantee that this minima is found, and the Graph Structure does not neccesarily allow for this kind of transformation(see Chapter <ref identities>), but training this kind of Network, does not result in the Model gaining any Classification Power. This can be seen in the corresponding Feature Map
<i f="aucmap1011" wmode="True">(1011..maybe not the best)aucmap for standart normated networks, showing nothing useful</i>
This migth seem now, as if there is a trivial solution: just reduce the compression size accordingly, but this has three problems
<list>
<e>First, it is not completely trivial to misuse the normation (Think of the standart deviation, there is a formula giving you Information about the 4th Value, given the first three, but even if we ignore the mean as beeing 0, this formula still involves squares and roots, which the Network has to learn, and even then, there are always two possibilities for the resulting Value.) so assuming that this is trivial, and that the Network will learn it garantueed would be wrong</e>
<e>Even if this is learned, this is not enough, it still has to compress this Information further and this can lead to Situations in which the Network has to decide between learning the easy Compression and the learning the interresting Compression</e>
<e>Trying to compress mean removed Information further is not as easy as compressing non removed Information. Think of two Values, for example 4 and 6, or, after setting the mean to be 0: -1 and 1. In 4 and 6 the Network can still decide to just average both Values and get an mediocre prediction, but if after removing the mean, the Network still averages both Values, the prediction zero is fairly useless<note Please note, that the difference between a good normation and a bad one is just physical Intuition. For example, we still set the mean of the Jet angles to be zero, just because the direction relative to the Measurement should be unimportant, but we would not set for example the maximum momentum to be 1, because this would remove something physically interresting>. This means, that simply subtracting each fixed Value from the compression size does not work, and that we would expect less good Classifier.</e>
</list>

So what we need, is a better way of normalising the Input Data. This Method should satisfy these conditions:
<list>
<e>Translation Invariance: #Eq(n(x),n(x+a))#</e>
<e>Scale Invariance:#Eq(n(x),n(a*x))#</e>
<e>No fixed features: you can not write any #f(x)# so that #Eq(f(n(x)),0)# for every #x# is given</e>
</list>

The first two Rules are obvious, since we want to use this, to remove any size Information, and third rule would solve the problem of an autoencoder focussing on normation artefacts.

All three Rules<note except for scale invariance with #LessThan(a,0)#> are solved by the following 3 Normation Steps (#x# is the Input, #n# the Output of the Normation method)

##Eq(y,x-mean(x))##
##Eq(z,y-mean(abs(y)))##
##Eq(n,z/(max(abs(y))+0.001))##

Here the definitions of #y# and #n# assert Translation and Scale Invariance respectively, while #n# and #z# remove any kind of Artefacts. Why they do this can be easily understand for #n#, by diving through the maximum value instead of the standart deviation, the only relation given is, that if none of the first three values is either #+1# or #-1#, the last Value is either #+1# or #-1#, but even if we ignore that this relation would be quite complicated to implement for a Neuronal Network <note there are uncertainities: could there be a value #0.997# reproduced, is there still a #1# remaining> and no way to differenciate between #1# and #-1# in general<note You could say, that this normation uses the Problem of mean reproducing Networks (Chapter <ref ameans>) to its benefit, by making the errors of a #1# or #-1# guessing Network bigger than every other possible distance, but this is a bit more complicated through #z#, since this kind of outputs have a clearly negative bias>. Also dividing through the maximum Value is generally a good Idea compared to dividing through the standart deviation, since it only divides by zero when every value is zero, and not when every Value is the same, which is more probable<note There is a small constant in our definition to remove this divergences, but this still removes some big differentials>. Also dividing by the maximum is generally a bit faster, while resulting in less nans (Chapter <ref nans>). The other definition is less easily understandable: First note, that it does not violate the first two rules, since #y# is already Translation Invariant, #z# is too, and since every 
#z(y)# is scale Invariant, Scale Invariance is also given<note You migth notice, that this is only the case for positive multiplicators, but we think this is an acceptable compromise for fullfilling Rule 3, and we guess you could also argue, that we only want to remove nonphysical Information, and since for #p_T# this is not a Problem (since #p_T# is positive and it has a peculiar shape), this is only interresting for angular Information, and P is broken>. Now consider only the definition of #z# and two different vectors #y# given by #Matrix([[1,0]])# and #Matrix([[1,1]])# ANDERE VEKTOREN?. Both result in the same first component, but different second components, so there is no Information from the first Value to the second, and thus Rule 3 is satisfied, and a Network using this kind of Normation has Classification Power:

<i f="aucmap928" wmode="True">(928..definitely take some top one)ABE for better norm</i>

<subsubsection title="Using this normation" label="usenorm">
Using this kind of Normation, at least 4 node Networks are invertible. And not only this, but also most Features are Invertible.
<i f="aucmap677" f2="aucmap928" wmode="True">invertible 4node network auc maps</i>
But the Quality suffers
<i f="none" wmode="True">(generate later for computational reasons)double roc curve for invertibility of normated networks</i>
and there are other consequences of the fact that this Network actually has to learn something nontrivial:
Firstly, we were forced to increase the Size of the compressed Feature Space from 5 to 9, this makes sense, since the Network that compares angles to zero, has to just reconstruct zeros in each Angle, and thus only has to save #p_T#<note And maybe flag, but as seen later in this Chapter and more in Chapter <ref oneoff>, this is usually not actually be the case>.
Also Networks, that before were very reproducable in their training<note they always just ignored the angles> are now less stable, and often vary their loss<note remember that the l2 loss goes quadratic in chances of the Inputs> over about one order of magnitude. Interrestingly, this variation shows a clear relation between the loss, and the Classification Quality
<i f="rtvl 0 2 4 5 6 7 8 9" wmode="True">(from c3pp atm)1k Networks, loss vs auc plot</i>
This relation is great, since it means, that finding a better autoencoder, automatically results in a better Classifier, and we thus can focus completely on improving the Autoencoder, and it can also justify the new compression size
<i f="none" wmode="True">(computationals)justification of the new compression size, auc vs quality for compression size 8 and 9</i>
since this is the first compression size, at which the Network becomes Invertible 
And the variation of the Networks can be solved by simply training longer:
In stead of the usual Method, of training until the loss does not improve itself for a set number of epochs(we used 30 before, but could probably have used less than 10), we now train first for a set number of epochs, and then enable the Stopping after the loss does no longer decrease. And as you see
<i f="none" wmode="True">(computational)comparison of reproducability for earlystopping and minepoch</i>
training for 1000 epochs, and then until the loss increases for 250 epochs, results in the most predictable, and qualitative results. In later Models, we reduce this to just stopping the training after 100 consecutive Epochs of not falling losses, to keep the Training time also managable.






<subsubsection title="Improving the Normation even further" label="normplus">
After seeing what an effect some kind of Normation can have, we are not completely satisfied anymore with the normated Feature Maps:
<i f="aucmap928" wmode="True">ABE for better norm (just a copy)</i>
consider the highest #p_T# Value (the lower rigth corner???????). While beeing the generally most interresting Particle, there is no Classification Power in it, and by looking at its distribution it immediatly becomes clear why
<i f="pt0draw928" wmode="True">(928/drawp0.py)better norm pt0 dist</i>
The Values are basically constant, so its Output has the same reconstruction as the flag Values (first collumn), from which we dont expect any physically useful Information.
So lets solve this: Since #lp_T# mostly has the same structure<note To be more precise, the difference between the first and the second Particle is higher than the difference between the last two ones>, Each Value gets divided by the first One, resulting in it always having the same Value. We solve this by replacing the definition of #n# to be:
##Eq(n,2*z/(max(abs(z))+mean(abs(z))))##

removing the need to set one Value to either positive or negative one, and thus making the highest Value in #lp_T# actually useful, and as you see, this works
<i f="aucmap534" wmode="True">ABE good norm</i>
But, as you also see, now the whole Classificatiuon Power lies in flag, and this should be quite confusing to you: Something having no physical Meaning beeing more useful than everything else. Not to different compared to Chapter <ref simplicity>.
This we will explain in Chapter <ref oneoff>.



