\contentsline {section}{Table of content}{1}{section*.1}%
\contentsline {section}{\numberline {1}Motivation}{3}{section.1}%
\contentsline {section}{\numberline {2}Introduction and literature}{5}{section.2}%
\contentsline {subsection}{\numberline {2.1}New physics }{5}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Neuronal networks and autoencoder }{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Graphs }{7}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Graph autoencoder }{9}{subsection.2.4}%
\contentsline {section}{\numberline {3}Basics}{11}{section.3}%
\contentsline {subsection}{\numberline {3.1}Binary classification }{11}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}ROC curve}{11}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Area under the curve}{13}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Datapreperation }{13}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Explaining graphics }{14}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Output images}{14}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}AUC Feature maps}{15}{subsubsection.3.3.2}%
\contentsline {section}{\numberline {4}A working graph autoencoder}{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}Graph neural networks }{17}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Tensorproducts}{17}{subsubsection.4.1.1}%
\contentsline {subsection}{\numberline {4.2}Compression algorithm }{18}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Decompression algorithm }{18}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Model setup }{18}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Our choice to not use biases}{19}{subsubsection.4.4.1}%
\contentsline {subsection}{\numberline {4.5}Losses }{19}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}$L_{2}$ loss}{20}{subsubsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.2}$L_{n}$ loss}{20}{subsubsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.3}Image like losses}{22}{subsubsection.4.5.3}%
\contentsline {subsection}{\numberline {4.6}Problems in evaluating a model }{23}{subsection.4.6}%
\contentsline {subsubsection}{\numberline {4.6.1}AUC scores}{23}{subsubsection.4.6.1}%
\contentsline {subsubsection}{\numberline {4.6.2}Losses}{24}{subsubsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.3}Images}{24}{subsubsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.4}oneoff width}{24}{subsubsection.4.6.4}%
\contentsline {subsection}{\numberline {4.7}Evaluating the autoencoder }{24}{subsection.4.7}%
\contentsline {subsubsection}{\numberline {4.7.1}4 nodes}{25}{subsubsection.4.7.1}%
\contentsline {subsubsection}{\numberline {4.7.2}9 nodes}{26}{subsubsection.4.7.2}%
\contentsline {subsection}{\numberline {4.8}Evaluating the classifier }{27}{subsection.4.8}%
\contentsline {subsubsection}{\numberline {4.8.1}4 nodes}{27}{subsubsection.4.8.1}%
\contentsline {subsubsection}{\numberline {4.8.2}9 nodes}{29}{subsubsection.4.8.2}%
\contentsline {section}{\numberline {5}Open questions}{31}{section.5}%
\contentsline {subsection}{\numberline {5.1}Scaling the network size }{31}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Problems in scaling}{31}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Scaling through batches}{31}{subsubsection.5.1.2}%
\contentsline {subsubsection}{\numberline {5.1.3}Scaling through dense networks}{32}{subsubsection.5.1.3}%
\contentsline {subsubsection}{\numberline {5.1.4}C addition}{33}{subsubsection.5.1.4}%
\contentsline {subsubsection}{\numberline {5.1.5}Scaling through losses}{35}{subsubsection.5.1.5}%
\contentsline {subsection}{\numberline {5.2}Simplicity and invertibility }{36}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Simplicity}{36}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Invertibility}{39}{subsubsection.5.2.2}%
\contentsline {section}{\numberline {6}Normalization}{41}{section.6}%
\contentsline {subsection}{\numberline {6.1}Solving invertibility through normalization }{41}{subsection.6.1}%
\contentsline {subsubsection}{\numberline {6.1.1}The meaning of complexity}{41}{subsubsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.2}How to normalise an autoencoder}{41}{subsubsection.6.1.2}%
\contentsline {subsubsection}{\numberline {6.1.3}Using this normalization}{43}{subsubsection.6.1.3}%
\contentsline {subsubsection}{\numberline {6.1.4}Improving the AUC scores for normalized networks}{47}{subsubsection.6.1.4}%
\contentsline {subsubsection}{\numberline {6.1.5}Scaling in normalized networks}{47}{subsubsection.6.1.5}%
\contentsline {subsubsection}{\numberline {6.1.6}Improving the normalization even further}{47}{subsubsection.6.1.6}%
\contentsline {section}{\numberline {7}Mixed networks}{50}{section.7}%
\contentsline {subsection}{\numberline {7.1}Oneoff networks }{50}{subsection.7.1}%
\contentsline {subsubsection}{\numberline {7.1.1}oneoff quality}{51}{subsubsection.7.1.1}%
\contentsline {subsection}{\numberline {7.2}Compressed oneoff learning }{52}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}A good classifier }{53}{subsection.7.3}%
\contentsline {subsubsection}{\numberline {7.3.1}Trained on QCD}{53}{subsubsection.7.3.1}%
\contentsline {subsubsection}{\numberline {7.3.2}Trained on top}{55}{subsubsection.7.3.2}%
\contentsline {subsection}{\numberline {7.4}Scaling for oneoff networks }{57}{subsection.7.4}%
\contentsline {subsubsection}{\numberline {7.4.1}In batches}{57}{subsubsection.7.4.1}%
\contentsline {subsubsection}{\numberline {7.4.2}Without batches}{58}{subsubsection.7.4.2}%
\contentsline {section}{\numberline {8}Applying this model to other datasets}{61}{section.8}%
\contentsline {subsection}{\numberline {8.1}Ligth dark matter }{61}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Other datasets }{64}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Quark or gluon}{64}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Leptons}{65}{subsubsection.8.2.2}%
\contentsline {subsection}{\numberline {8.3}Cross comparisons }{66}{subsection.8.3}%
\contentsline {section}{\numberline {9}Conclusion}{69}{section.9}%
\contentsline {subsection}{\numberline {9.1}Whats next? }{69}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Acknowledgements }{71}{subsection.9.2}%
\contentsline {section}{Appendices}{71}{subsection.9.2}%
\contentsline {section}{\numberline {A}Understanding specific choices}{72}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Changing the input feature space }{72}{subsection.A.1}%
\contentsline {subsection}{\numberline {A.2}Is it a good idea to relearn the graph at each step? }{73}{subsection.A.2}%
\contentsline {subsection}{\numberline {A.3}The consequences of sorting outputs by lpt }{73}{subsection.A.3}%
\contentsline {subsection}{\numberline {A.4}The usage of a batchNormalization layer in the middle of the gae }{75}{subsection.A.4}%
\contentsline {subsection}{\numberline {A.5}Changing the definition of the transverse momentum }{76}{subsection.A.5}%
\contentsline {subsection}{\numberline {A.6}Comparing our graph update layer to particleNet }{77}{subsection.A.6}%
\contentsline {section}{\numberline {B}Experiments using graph autoencoder}{78}{appendix.B}%
\contentsline {subsection}{\numberline {B.1}Comparing our graph update layer to particleNet }{78}{subsection.B.1}%
\contentsline {subsection}{\numberline {B.2}Variating the compression size }{78}{subsection.B.2}%
\contentsline {subsection}{\numberline {B.3}Things we learned from implementing a gae in tensorflow and keras }{79}{subsection.B.3}%
\contentsline {subsubsection}{\numberline {B.3.1}overflow in angular differences, and how to solve it}{79}{subsubsection.B.3.1}%
\contentsline {subsubsection}{\numberline {B.3.2}how to deal with NANs}{80}{subsubsection.B.3.2}%
\contentsline {subsubsection}{\numberline {B.3.3}Why relus are great}{80}{subsubsection.B.3.3}%
\contentsline {subsection}{\numberline {B.4}Metrik analysis }{81}{subsection.B.4}%
\contentsline {subsubsection}{\numberline {B.4.1}How topK works exactly}{81}{subsubsection.B.4.1}%
\contentsline {subsubsection}{\numberline {B.4.2}How topK might actually not be the best idea}{83}{subsubsection.B.4.2}%
\contentsline {subsection}{\numberline {B.5}Trainingsize, and why graph autoencoder don`t care about it }{83}{subsection.B.5}%
\contentsline {subsection}{\numberline {B.6}Why autoencoder reproduce mean values }{85}{subsection.B.6}%
\contentsline {section}{\numberline {C}Overview of less useful networks}{87}{appendix.C}%
\contentsline {subsection}{\numberline {C.1}Failed approaches }{87}{subsection.C.1}%
\contentsline {subsubsection}{\numberline {C.1.1}trivial models}{87}{subsubsection.C.1.1}%
\contentsline {subsubsection}{\numberline {C.1.2}minimal models}{87}{subsubsection.C.1.2}%
\contentsline {subsection}{\numberline {C.2}An explicit look at the first working graph autoencoder }{88}{subsection.C.2}%
\contentsline {subsubsection}{\numberline {C.2.1}topK}{88}{subsubsection.C.2.1}%
\contentsline {subsubsection}{\numberline {C.2.2}Compression}{88}{subsubsection.C.2.2}%
\contentsline {subsubsection}{\numberline {C.2.3}Decompression}{89}{subsubsection.C.2.3}%
\contentsline {subsubsection}{\numberline {C.2.4}Sorting}{89}{subsubsection.C.2.4}%
\contentsline {subsubsection}{\numberline {C.2.5}Training setup}{89}{subsubsection.C.2.5}%
\contentsline {subsubsection}{\numberline {C.2.6}Results}{90}{subsubsection.C.2.6}%
\contentsline {subsection}{\numberline {C.3}Improving autoencoder }{92}{subsection.C.3}%
\contentsline {subsubsection}{\numberline {C.3.1}Training setup}{92}{subsubsection.C.3.1}%
\contentsline {subsubsection}{\numberline {C.3.2}Results}{93}{subsubsection.C.3.2}%
\contentsline {subsection}{\numberline {C.4}Improving autoencoder even further? }{95}{subsection.C.4}%
\contentsline {subsubsection}{\numberline {C.4.1}Physical intuition behind the encoding algorithm}{95}{subsubsection.C.4.1}%
\contentsline {subsubsection}{\numberline {C.4.2}better encoding}{96}{subsubsection.C.4.2}%
\contentsline {subsubsection}{\numberline {C.4.3}better decoding}{97}{subsubsection.C.4.3}%
\contentsline {subsection}{\numberline {C.5}The compression algorithm that we would wish to have }{98}{subsection.C.5}%
\contentsline {section}{\numberline {D}More problems while writing a graph autoencoder}{101}{appendix.D}%
\contentsline {subsection}{\numberline {D.1}Choosing the rigth compression size }{101}{subsection.D.1}%
\contentsline {subsection}{\numberline {D.2}Building identities out of graphs }{101}{subsection.D.2}%
\contentsline {subsection}{\numberline {D.3}Is permutation invariance good or bad? }{103}{subsection.D.3}%
\contentsline {subsection}{\numberline {D.4}Why use graph autoencoder }{103}{subsection.D.4}%
\contentsline {subsection}{\numberline {D.5}Why not to use graph autoencoder }{104}{subsection.D.5}%
\contentsline {subsubsection}{\numberline {D.5.1}Reproduding vs classifing quality}{104}{subsubsection.D.5.1}%
\contentsline {subsubsection}{\numberline {D.5.2}General problems}{109}{subsubsection.D.5.2}%
\contentsline {section}{\numberline {E}Understanding Oneoff networks with more precision}{111}{appendix.E}%
\contentsline {subsection}{\numberline {E.1}Other algorithms }{111}{subsection.E.1}%
\contentsline {subsubsection}{\numberline {E.1.1}Support vector machines}{111}{subsubsection.E.1.1}%
\contentsline {subsubsection}{\numberline {E.1.2}k neirest neighbours}{112}{subsubsection.E.1.2}%
\contentsline {subsubsection}{\numberline {E.1.3}Isolation forests}{112}{subsubsection.E.1.3}%
\contentsline {subsection}{\numberline {E.2} }{112}{subsection.E.2}%
\contentsline {subsubsection}{\numberline {E.2.1}SVM}{113}{subsubsection.E.2.1}%
\contentsline {subsubsection}{\numberline {E.2.2}Isolation forest}{113}{subsubsection.E.2.2}%
\contentsline {subsubsection}{\numberline {E.2.3}k neirest neighbour}{113}{subsubsection.E.2.3}%
\contentsline {subsubsection}{\numberline {E.2.4}oneoff}{113}{subsubsection.E.2.4}%
\contentsline {subsubsection}{\numberline {E.2.5}oneoff math}{113}{subsubsection.E.2.5}%
\contentsline {subsection}{\numberline {E.3}Self improving oneoff networks }{115}{subsection.E.3}%
\contentsline {subsubsection}{\numberline {E.3.1}oneoff outside of physics}{117}{subsubsection.E.3.1}%
\contentsline {subsubsection}{\numberline {E.3.2}Some physical interpretability for oneoff networks}{119}{subsubsection.E.3.2}%
\contentsline {subsection}{\numberline {E.4}How a oneoff network can become noninvertible }{119}{subsection.E.4}%
\contentsline {subsection}{\numberline {E.5}Why c addition might not be perfect }{121}{subsection.E.5}%
\contentsline {subsection}{\numberline {E.6}Treating oneoff networks as observables }{121}{subsection.E.6}%
\contentsline {section}{\numberline {F}Other usecases for grapa}{123}{appendix.F}%
\contentsline {subsection}{\numberline {F.1}Fraud detection for social networks }{123}{subsection.F.1}%
\contentsline {subsubsection}{\numberline {F.1.1}Datageneration}{123}{subsubsection.F.1.1}%
\contentsline {subsubsection}{\numberline {F.1.2}Training}{124}{subsubsection.F.1.2}%
\contentsline {subsubsection}{\numberline {F.1.3}Whats next}{125}{subsubsection.F.1.3}%
\contentsline {subsection}{\numberline {F.2}Accelarating molecular networks through pooling }{125}{subsection.F.2}%
\contentsline {subsubsection}{\numberline {F.2.1}Datageneration}{126}{subsubsection.F.2.1}%
\contentsline {subsubsection}{\numberline {F.2.2}Training}{126}{subsubsection.F.2.2}%
\contentsline {subsubsection}{\numberline {F.2.3}Whats next?}{126}{subsubsection.F.2.3}%
\contentsline {subsection}{\numberline {F.3}High level machine learning and feynman diagramms }{127}{subsection.F.3}%
\contentsline {subsubsection}{\numberline {F.3.1}Data generation}{127}{subsubsection.F.3.1}%
\contentsline {subsubsection}{\numberline {F.3.2}Training}{128}{subsubsection.F.3.2}%
\contentsline {subsubsection}{\numberline {F.3.3}Whats next?}{131}{subsubsection.F.3.3}%
\contentsline {subsection}{\numberline {F.4}Graph like generators and onoff initializers }{132}{subsection.F.4}%
\contentsline {subsubsection}{\numberline {F.4.1}Data generation}{133}{subsubsection.F.4.1}%
\contentsline {subsubsection}{\numberline {F.4.2}Training}{133}{subsubsection.F.4.2}%
\contentsline {subsubsection}{\numberline {F.4.3}Whats next?}{134}{subsubsection.F.4.3}%
\contentsline {section}{\numberline {G}Additional Figures}{135}{appendix.G}%
\contentsline {section}{List of Figures}{139}{section*.142}%
\contentsline {section}{List of Tables}{143}{section*.143}%
\contentsline {section}{References}{143}{section*.144}%
