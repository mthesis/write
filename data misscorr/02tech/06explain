<subsection title="Explaining Graphics" label="graphics">

<subsubsection title="Output Images" label="imgout">
<i f="simpledraw7638_200" wmode="True">a sample reconstuction image</i>
In those images, you see the #phi# and #eta# Value of each particle, including any normalization, plotted once for the input jet in red, and once for the output jet in blue. This means that a perfect network would show both jets overlapping in violet. Zero Particles are not shown and there is some indication of the transverse momentum in the size of the dots, which is given proportional to #1+9/(1+lp_T)#<note inverse function, since higher #p_T# result in lower #lp_T#, and some constants to keep the radius finite>. This sadly does allow you to see differences in #lp_T# very well, so we also have to look only at #lp_T#. We show those here as a function of the index.
<i f="ptdraw200_7638" wmode="True">A sample momentum reconstruction image</i>
This way of looking at the Network performance is quite useful for finding patterns in the data. There seem to be Networks that show a high correlation between the angles<note see for example Appendix <ref firstworking>>, and it is quite common for the reproduced Values to have less spread than the input one <note see Appendix <ref secondworking>>. A problem here is, that you can only look at some Images, and finding one nicely looking reproduction for each Network is not that hard. To compat this, we use always the same event<note the same event for each training set to be precise>.

<subsubsection title="AUC Feature Maps" label="imgmaps">
<i f="aucmap200" wmode="True">A sample AUC Featuremap</i>
A L2 loss is just a mean over a lot of L2 losses for each Feature and Particle. So you could just not average them, to also be able to calculate an AUC score for each Feature and Particle. These AUC scores are shown in Feature Maps, showing the Quality of each combination of Feature on the horizontal axis and Particle on the vertical axis in the form of pixels. A perfect classifier(#Eq(AUC,1)#) Would result in a dark blue pixel, a perfect anti classifier(#EQ(AUC,0)#) Would be represented by a dark red pixel. Finally, a useless classifier, that guesses if a jet is part of background or signal(Or one that always results in the same value)(#Eq(AUC,1/2)#) Would be a white classifier. In short: The more colorful a pixel is, the better it is, and an Autoencoder trained on qcd events should have a feature map that is blue, while an Autoencoder trained on top events should be represented in red consistently.
The nice thing about those maps, is that they can show the focus of the Network, as well as its problems. Since a perfect reconstruction as well as a terrible one, has no decision power, a Network that has focus problems(Meaning it reconstructs some things much better than other things, making both parts worse), can be clearly seen in those maps. Also, it is fairly common, to get one feature and Particle, that has more decision power, than the whole combined Network (see <ref oneoff> for an example and <ref caddition> For the explanation). Finally, an AUC map that is completely blue or red is quite uncommon, more probably some Features are red, some are blue, and you get an indication on which Features are useful for the current Task <note see <ref firstworking>>.

<ignore>
<subsubsection title="Network Setups" label="imgsetups">
<i f="vis900" wmode="True">(900/vis)a sample Network Setup Image</i>
We show the Setup of each Network as Images similar to those<note these Images are not made for you to be able to write the whole Network yourself, but to understand some of the parts that are switched out between different iterations. For a more technical model description, look at the Images keras generates. See for this <ref amodelimg> And in the code at createmodel.py or at encoder.Png and decoder.png>. The Information travels from the left side to the right side, through a lot of layers, which all have their own symbol. Some symbols are explained at the Point where they are used, but here those that are shown in Image <refi vis900> From left to right
<list>
<e>The Data that is trained on</e>
<e>Is getting preprocessed into 4 variables (see Chapter <ref data> For more Information)</e>
<e>Afterwards it is sorted by its #lpt#<note This is done to make comparing Jets easier, it is not strictly neccesary, but makes the training more effective (see Appendix <ref asort), also the first sorting is really not needed, since the particles are already sorted, but applying it here means, that you dont have to take care while chancing the preprocessing of #lpt#></e>
<e>Afterwards, each Event is normalized (see Chapter <ref normalization> For the why and how)</e>
<e>After normalization, these are the Values, we want to reconstruct, as marked by the line on the top, and another normalization, this time a standard BatchNormalization, is applied. This allows the Network to learn the desired Scale and Mean for the remaining layers(see Appendix <ref abatchnorm> For why this is done)<note It is fairly important to add the BatchNormalization layer after the Value for comparison is choosen, since a learnable scale, would else just result in the Network perfectly reconstructing zeros></e>
<e>After the normalization, a topK layer generates a Graph to the particle nodes(see Appendix <ref atopkhow> For a breakdown on how this layer works)</e>
<e>Which is used in a graph update layer (see Chapter <ref gnn></e>
<e>And to compress the Graph (see Chapter <ref encoding0>)</e>
<e>This is the Point, where the encoder Part stops, and is followed by another Layer, decompressing the Graph again (see Chapter <ref decoding0>)</e>
<e>as well as a new graph update step</e>
<e>and finally another sort, matching the sort before the graph Update Step, after which the current jet is compared to the original one</e>
</list>
As you see, this is a relatively easy Graph Autoencoder, but explaining each layer takes some time, which is why we use this graphic to help us.
</ignore>