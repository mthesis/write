<ignore>
I did not spend much (enough) time making thinks sound nice, and I dont think I will use handwritten graphs (but the libraries to plot graphs are kinda terrible and I think its ok for now)
<section About Decoders>

<subsection mathematical description of a graph>
To work numerically with a Graph, you can use the Adjacency Matrix of it, its Entry #A_ij# are #1# if there is a connection between the nodes #i# and #j#<note This is a bit of a simplification, since the order of nodes does not matter, You can say that multiplication with any Permutationmatrix does not neccesarily keep the Adjacency Matrix invariant, but still represents the same graph. I will try order my graphs from left to rigth or I`ll enumerate them> and #0# if not. You can extend this by replacing the #1# with a Value defining the strength of a connection or by transforming the Graph into a directed graph by removing the Symmetry of #A# (#A_ij# is one exactly if there is a connection from #i# to #j#), but both of those extensions do not chance much when we talk about decompression. Finally, there are the diagonal entries #A_ii# of the Adjacency Matrix, that are not jet defined. Classically you interpret them as a Node that is connected to it self, but here this does not neccesary matter, since a connection from each node to it self, just couples the self interaction part of the update process with the neigbour interaction one, so in the learning phase, this difference will be made redundant. I will keep them to be one consistently in my examples, because I think that decompression algorithms look a bit nicer then.
<i f="basic_graphs" wmode=True>a) a simple 3 node graph, b) a 2 node graph, c) a weigthed graph, d) a directed graph</i>

<subsection what is a graph of a graph>
To think about what it means, to have a graph of a graph, consider the following example: Lets say there is a room, with a lot of people inside. Each of them have one of three jobs: there are some Fishermen, some Doctors and a couple Economists. In this Group of People, each Doctor likes to talk to anybody (and the other People are happy to do so), but Economists and Fishermen really dont like each other, while also everybody likes to talk to people with the same job<note so the diagonal elements of the Adjacency Matrix are one>. You can now use a graph to map those relations, and since we only have three kinds of people for now, this graph has three nodes (see the graph a below). Now lets introduce a second attribute for each person, maybe something like a popular movie, that the first half has seen and wants to talk about, which annoyes the second half. So you get a second graph (graph b below) with two kinds of people, who want to talk to each other, but never to the other group.
So we now have two graphs describing the situation if each person would only have one attribute, but can we describe the whole situation with a graph? and can we define a function to go from two partial Adjacency Matrices to a complete one? Yes (graph c), by just utilising the given rules: two people want to talk to each other, if both "attributes" want to talk to each other, or (using #n_1# as the number of nodes of the first graph)
##Eq(A(i_1+n_1*i_2,j_1+n_1*j_2),A(i_1,j_1)*A(i_2,j_2))##
so basically each new connection is the logical and of both old connections
<i f="group_graphs" wmode=True></i>
lets try this on some examples
<i f="combine_graphs" wmode=True></i>
As you see, it is not neccesary trivial to find patterns. This is because of the Permutation invariance of the Adjacency Matrix. But some notes: 
<list>
<e>two fully connected graphs produce a fully connected one</e>
<e>if one of the two graphs is not connected (it is not true, that you can go from each node to each other one) the resulting graph is also not connected</e>
<e>symmetry of the Adjacency Matrix is conserved</e>
<e>the product of a graph with #n_1# nodes with one with #n_2# nodes has #n_1*n_2# nodes</e>
</list>
If you reorder the nodes between each other, you can show, that the resulting Adjacency Matrix is the Kroneckerproduct (I have learned it under the name Tensorproduct) of the original Matrices. Please note, that here it makes a difference if each note is connected to each other, so the resulting graph operation is technically called the strong product of two graphs, but the numerical Operation stays the same, and so the difference between the strong product and the Tensorproduct becomes a hyperparameter<note Also there are other kinds of Operations, that combine graphs in a similar manner, I should name the Cartesian Product, which is achieved by an or in the original example instead of an and (and no self connectivity), and I have experimented with another kind of Operation based less on the Adjancency Matrix, but more on the Visualisation before I found Tensorproducts.  I decided to use Tensorproducts because there a bit more easy to implement and work also when the diagonal Elements are not all one>.

<subsection why I am not completely happy with simple Tensorproducts>
First, to run this Operation, I can only use two graphs, that means, if I say that I decompress a graph by setting each node to a new graph, the new graph has to be the same for each node, so the new graph is a function of the whole old graph. This is not only maybe not general enough, but it also can (and currently does) break graph symmetry<note permutation invariance of the nodes>, when you define your graph as a learnable (dense) function of each nodes attributes<note you could do something like let this function work on a the mean of each nodes attributes or you could replace the dense function by a graph network, but those restrict information or increase the complexity of the network a lot, respectively>
My second Problem is about the possible Results. If we ignore Permutation Invariance, there are #2**((n**2-n)/2)# possible Adjacency Matrices with #n# nodes, so for a simple product of for example a 2 graph and a 3 graph, there are #2# times #8# possibilities but #2**15# possible Matrices of the resulting size, so most of graphs simply cannot be the result of a decompression<note if you consider permuation invariance the story is mostly the same>. It should be noted that this looks worse than it actually is, since a graph beeing mostly the same produces mostly the same output, but a little more accurate Graphs would still be probably nice.

<subsection How to fix this>
What i want to do, is to allow each node to choose its own graph. The question is just how:
Start with an empty matrix of size #n_1*n_2#. Split this Matrix in #n_1**2# submatrices of size #n_2#. Set the diagonal Elements of this general Matrix (which elements are still Matrices) to the new Matrices, which you learn by their parameters. Now to the offdiagonal Elements, here I have two solutions:
<list>
<e>The Graph #A_ij# is a function of the Graphs #A_ii# and #A_jj# (something easy like nodes are connected if both are connected (this conserves symmetry), but you could also make this learnable). This method I call graphlike</e>
<e>Instead of learning the Graph #A_ii# from the attributes #x_i# you learn the Graph #A_ij# from the attributes of #x_i# and #x_j#. (You could also train on #x_i+x_j# and #abs(x_i-x_j)# to enforce symmetry of the resulting matrix, but this is actually not neccesary to keep the permutation invariance, only to keep the graph symmetric <note please note that topK does not neccesary generate symmetric graphs>. This method i call paramlike</e> 
</list>
Finally to combine this new graph with the old Graph, each subgraph is multiplied with the Value of the corresponding connection in the old Graph.
In both cases, there are more Graphs possible. Consider again 2 times 3, then here are 2 graphs of size 3 multiplied with a 2 size graph, resulting in #2**7# possibilities, instead of #2**4#. This is still not exactly #2**15# but this still results in a bit more accurate Graphs<note you could actually argue, that at some point, more accurate Graphs hurt the reconstruction, the idea here would be something like you want to force the network to be able to work with more abstract Matrices and not to focus to much on little differences between Matrices>.







</ignore>









